#!/usr/bin/bash
wsdl2h -y -t typemap.dat OrdersManager.wsdl

rm -rv Service/etc/gsoap/gen/*
soapcpp2 -jSL -d Service/etc/gsoap/gen/ OrdersManager.h

rm -rv Client/etc/gsoap/gen/*
soapcpp2 -jCL -d Client/etc/gsoap/gen/ OrdersManager.h
